#!/usr/bin/env node

/* jshint esversion: 8 */
/* global it, xit, describe, before, after, afterEach */

'use strict';

require('chromedriver');

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    { Builder, By, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

if (!process.env.USERNAME || !process.env.PASSWORD) {
    console.log('USERNAME and PASSWORD env vars need to be set');
    process.exit(1);
}

describe('Application life cycle test', function () {
    this.timeout(0);

    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };
    const LOCATION = process.env.LOCATION || 'test';
    const TEST_TIMEOUT = 30000;

    const serverPassword = 'testServerPasswordString12345';

    let app, browser;

    before(async function () {
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
        if (!fs.existsSync('./screenshots')) fs.mkdirSync('./screenshots');
    });

    after(function () {
        browser.quit();
    });

    afterEach(async function () {
        if (!process.env.CI || !app) return;

        const currentUrl = await browser.getCurrentUrl();
        if (!currentUrl.includes(app.domain)) return;
        expect(this.currentTest.title).to.be.a('string');

        const screenshotData = await browser.takeScreenshot();
        fs.writeFileSync(`./screenshots/${new Date().getTime()}-${this.currentTest.title.replaceAll(' ', '_')}.png`, screenshotData, 'base64');
    });

    async function clearCache() {
        await browser.manage().deleteAllCookies();
        await browser.quit();
        browser = null;
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        chromeOptions.addArguments(`--user-data-dir=${await fs.promises.mkdtemp('/tmp/test-')}`); // --profile-directory=Default
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
    }

    function getAppInfo() {
        const inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location.indexOf(LOCATION) === 0; })[0];
        expect(app).to.be.an('object');
    }

    async function waitForElement(elem) {
        await browser.wait(until.elementLocated(elem), TEST_TIMEOUT);
        await browser.wait(until.elementIsVisible(browser.findElement(elem)), TEST_TIMEOUT);
    }

    async function bootstrap() {
        await clearCache();

        await browser.get('about:blank');
        await browser.sleep(2000);

        await browser.get(`https://${app.fqdn}/bootstrap`);

        // Set server password (1st input is the password, 2nd input is to confirm the password)
        await waitForElement(By.xpath('//input[@type="password"][1]'));
        await browser.findElement(By.xpath('//input[@type="password"][1]')).sendKeys(serverPassword);
        await browser.findElement(By.xpath('//input[@type="password"][2]')).sendKeys(serverPassword);
        await browser.findElement(By.xpath('//button[.="OK"]')).click();
        await browser.sleep(4000);
    }

    async function setup() {
        await clearCache();

        await browser.get('about:blank');
        await browser.sleep(2000);

        await browser.get(`https://${app.fqdn}`);
        await browser.sleep(3000);
        await waitForElement(By.xpath("//input[@type='password']"));
        await browser.findElement(By.xpath("//input[@type='password']")).sendKeys(serverPassword);
        await browser.findElement(By.xpath('//button[contains(.,"Sign in")]')).click();

        // Click start fresh.
        await waitForElement(By.xpath("//div/button[normalize-space() = 'Start fresh']"));
        await browser.findElement(By.xpath("//div/button[normalize-space() = 'Start fresh']")).click();

        // Add account
        await waitForElement(By.xpath("//div/button[normalize-space() = 'Add account']"));
        await browser.findElement(By.xpath("//div/button[normalize-space() = 'Add account']")).click();

        // Create local account
        await waitForElement(By.xpath("//button[contains(., 'local account')]"));
        await browser.findElement(By.xpath("//button[contains(., 'local account')]")).click();

        // Add local account name and balance
        await waitForElement(By.xpath('//form/label/input[@name="name"]'));
        await browser.findElement(By.xpath('//form/label/input[@name="name"]')).sendKeys("Checking Account");
        await browser.findElement(By.xpath('//form/label/input[@name="balance"]')).sendKeys("1234.56");
        await waitForElement(By.xpath("//form/div/button[normalize-space()='Create']"));
        await browser.findElement(By.xpath("//form/div/button[normalize-space()='Create']")).click();
        await waitForElement(By.xpath("//span[normalize-space() = '1,234.56']"));
    }

    async function login() {
        await browser.get(`https://${app.fqdn}/login`);

        await waitForElement(By.xpath("//input[@type='password']"));
        await browser.findElement(By.xpath("//input[@type='password']")).sendKeys(serverPassword);
        await browser.findElement(By.xpath('//button[contains(.,"Sign in")]')).click();

        await waitForElement(By.xpath("//button[contains(., 'Server online')]"));
    }

    async function logout() {
        await browser.get('about:blank');
        await browser.sleep(2000);
        await clearCache();
    }

    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });

    it('install app', function () { execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS); });
    it('can get app information', getAppInfo);
    it('bootstrap', bootstrap);
    it('first time setup', setup);
    it('can logout', logout);

    it('can restart app', async function () {
        execSync(`cloudron restart --app ${app.id}`, EXEC_ARGS);
        await browser.sleep(3000);
    });

    it('can login', login);
    it('can logout', logout);

    it('backup app', function () { execSync(`cloudron backup create --app ${app.id}`); });
    it('restore app', async function () {
        const backups = JSON.parse(execSync(`cloudron backup list --raw --app ${app.id}`));
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
        execSync('cloudron install --location ' + LOCATION, EXEC_ARGS);
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
        await browser.sleep(3000);
    });
    it('can login', login);
    it('can logout', logout);

    it('move to different location', async function () {
        execSync(`cloudron configure --app ${app.id} --location ${LOCATION}2`, EXEC_ARGS);
        getAppInfo();
    });
    it('can login', login);
    it('can logout', logout);

    it('uninstall app', async function () {
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });

    // Test app update
    it('can install app for update', function () { execSync(`cloudron install --appstore-id org.actualbudget.cloudronapp --location ${LOCATION}`, EXEC_ARGS); });

    it('can get app information', getAppInfo);
    it('clear cache', clearCache);
    it('bootstrap', bootstrap);
    it('first time setup', setup);
    it('can logout', logout);
    it('can update', async function () {
        execSync(`cloudron update --app ${LOCATION}`, EXEC_ARGS);
    });
    it('can get app information', getAppInfo);
    it('can login', login);
    it('can logout', logout);

    it('uninstall app', () => execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS));
});
