FROM cloudron/base:5.0.0@sha256:04fd70dbd8ad6149c19de39e35718e024417c3e01dc9c6637eaf4a41ec4e596c

RUN mkdir -p /app/code /app/pkg
WORKDIR /app/code

ENV NODE_ENV=production

# renovate: datasource=github-tags depName=actualbudget/actual versioning=semver extractVersion=^v(?<version>.+)$
ARG ACTUAL_VERSION=25.3.1
RUN curl -L https://github.com/actualbudget/actual/archive/refs/tags/v${ACTUAL_VERSION}.tar.gz | tar -xz --strip-components 1 -f -

RUN npm install -g yarn && \
    yarn workspaces focus --all --production

ADD start.sh config.json.template /app/pkg/

# Softlink /data to /app/data, because Actual hardcodes the default dataDir location as /data
# https://github.com/actualbudget/actual-server/blob/933fc27126903d748edfb330929f3bc0394d0072/src/load-config.js#L13C1-L13C69
RUN ln -s /app/data /data

CMD [ "/app/pkg/start.sh" ]
